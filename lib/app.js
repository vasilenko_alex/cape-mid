var debug = require('debug')('cape:index');
var path = require('path');
var url = require('url');
var koa = require('koa');
var mount = require('koa-mount');
var koaBody = require('koa-better-body');
var gzip = require('koa-gzip');
var config = require('./config');
var rest = require('./middlewares/rest-middleware');

var app = koa();

app.use(gzip());
app.use(koaBody());
app.use(rest());

// API
app.use(mount('/api/v1', require('./controllers/v1/api')));

app.listen(config.port);

debug('Server listening @ %s', config.port);
