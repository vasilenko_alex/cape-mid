var debug = require('debug')('cape:rest');

module.exports = function() {
  return function*(next) {
    try {
      yield next;
      if (this.result) {
        this.body = this.result;
      }
    } catch (err) {
      debug(err);
      this.body = err;
    }
  };
};
