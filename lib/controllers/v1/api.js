var debug = require('debug')('cape:api-v1');
var Router = require('koa-router');
var request = require('koa-request');
var path = require('path');
var google = require('googleapis');
var _ = require('lodash');

var controller = new Router();
var customsearch = google.customsearch('v1');

const CX = '016322080484712764574:aor-b4k4ngu';
const API_KEY = 'AIzaSyDsJRkK4-TYf9MQo4DwN-x8s7PC2Pnu7Kg';

// Custom google search
controller.get('/google/:phrase', function*() {
  var self = this;

  function googleGen(callback) {
    customsearch.cse.list({cx: CX, q: this.params.phrase, auth: API_KEY }, function(err, resp) {
      callback(err, _.map(resp.items, function (item) {
        return item.title
      }));
    });
  }

  this.result = yield googleGen;
});

// Hacky google autocomplete service
controller.get('/google/auto/:phrase', function*() {
  var response = yield request({
    url: 'http://suggestqueries.google.com/complete/search?q=' + this.params.phrase + '&client=firefox&hl=en'
  });

  this.result = response.body
});

module.exports = controller.middleware();
